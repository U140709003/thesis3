package com.example.esra.last;

public class User {
    private String userName;
    private String currentTime;


    public User(String userName, String currentTime) {
        this.userName = userName;
        this.currentTime = currentTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }
}
