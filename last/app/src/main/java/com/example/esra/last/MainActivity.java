package com.example.esra.last;

import android.content.Intent;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;


public class MainActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback,Start.StartFragmentListener,Register.RegisterFragmentListener,HomePage.HomePageFragmentListener,Settings.OnFragmentInteractionListener {
    EditText mEditText;
    TextView mEditText2;
    TextView mTextView;
    FrameLayout content;
    Fragment currentFragment = null;
    FragmentTransaction ft;
    private Fragment fragment;
    BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditText = findViewById(R.id.editText);
        mEditText2 = findViewById(R.id.editText2);
        mTextView = findViewById(R.id.textView);
        content = findViewById(R.id.content);
        ft = getSupportFragmentManager().beginTransaction();
        currentFragment = new Start();
        ft.replace(R.id.content, currentFragment);
        ft.commit();
        fragment = new HomePage();
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        navigation.setVisibility(View.GONE);
        mTextView.setVisibility(View.GONE);
        mEditText.setVisibility(View.GONE);
        mEditText2.setVisibility(View.GONE);


        NfcAdapter mAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mAdapter == null) {
            mEditText.setText("Sorry this device does not have NFC.");
            return;
        }

        if (!mAdapter.isEnabled()) {
            Toast.makeText(this, "Please enable NFC via Settings.", Toast.LENGTH_LONG).show();
        }

        mAdapter.setNdefPushMessageCallback(this, this);

    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    currentFragment = new HomePage();
                    switchFragment(currentFragment);
                    return true;
                case R.id.navigation_payments:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.remove(currentFragment);
                    ft.commit();
                    mTextView.setVisibility(View.VISIBLE);
                    mEditText.setVisibility(View.VISIBLE);
                    mEditText2.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_settings:
                    currentFragment = new Settings();
                    switchFragment(currentFragment);
                    mEditText.setVisibility(View.GONE);
                    mTextView.setVisibility(View.GONE);
                    mEditText2.setVisibility(View.GONE);
                    return true;

            }
            return false;
        }
    };
    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        String message = mEditText.getText().toString();
        Log.d("NFCDemo", "Sending message:" + message);
        NdefRecord ndefRecord = NdefRecord.createMime("text/plain", message.getBytes());
        NdefMessage ndefMessage = new NdefMessage(ndefRecord);
        return ndefMessage;
    }

    public void loginClicked(){
        Log.d("MainActivity", "loginClicked");
        //((Start)currentFragment).txtRegister.getText();
        if(((Start)currentFragment).uname.getText().toString().equalsIgnoreCase("passenger") &&((Start)currentFragment).pass.getText().toString().equals("123")){
            ft = getSupportFragmentManager().beginTransaction();
            currentFragment = new HomePage();
            ft.replace(R.id.content, currentFragment);
            ft.commit();
            navigation.setVisibility(View.VISIBLE);
        }else if(((Start)currentFragment).uname.getText().toString().equalsIgnoreCase("driver") && ((Start)currentFragment).pass.getText().toString().equals("123")){
            Intent intent = new Intent(MainActivity.this, NFCDisplayActivity.class);
            startActivity(intent);
            //navigation.setVisibility(View.VISIBLE);
        }
        else{
            //Toast.makeText((AppCompatActivity)mListener,"Login failed",Toast.LENGTH_LONG);
        }

    }


    private void registerClicked() {
        Log.d("MainActivity", "registered");
        ft = getSupportFragmentManager().beginTransaction();
        currentFragment = new Register();
        ft.replace(R.id.content, currentFragment);
        ft.commit();
        mEditText.setVisibility(View.GONE);
        mTextView.setVisibility(View.GONE);
        mEditText2.setVisibility(View.GONE);
    }

    private void registerRemove() {
        ft = getSupportFragmentManager().beginTransaction();
        ft.remove(currentFragment);
        ft.commit();
    }
    @Override
    public void registerFromRegisterClicked() {
        registerRemove();
    }

    @Override
    public void registerFromStartClicked() {
        registerClicked();
    }


    @Override
    public void fromHomeFragment() {
        registerRemove();
        mTextView.setVisibility(View.VISIBLE);
        mEditText.setVisibility(View.VISIBLE);
        mEditText2.setVisibility(View.VISIBLE);
    }
    public void switchFragment(Fragment fr){
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content, fr);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
